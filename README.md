# EKS

Create a **EKS** cluster with **EKSCTL** using Cluster Autoscaling and other cool stuff...

## Install Chocolatey 

Know the Requirements:
Windows 7+ / Windows Server 2003+
PowerShell v2+ (minimum is v3 for install from this website due to TLS 1.2 requirement)
.NET Framework 4+ (the installation will attempt to install .NET 4.0 if you do not have it installed)(minimum is 4.5 for install from this website due to TLS 1.2 requirement)

With PowerShell, you must ensure Get-ExecutionPolicy is not Restricted. We suggest using Bypass to bypass the policy to get things installed or AllSigned for quite a bit more security.

Run Get-ExecutionPolicy. If it returns Restricted, then run Set-ExecutionPolicy AllSigned or Set-ExecutionPolicy Bypass -Scope Process.


Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

Paste the copied text into your shell and press Enter.

Wait a few seconds for the command to complete.


# Install AWS CLI 

## Install AWS CLI using Chocolatey

Using Powershell with Administrator Mode

Choco install awscli -y


## Install AWS CLI using Linux 
```bash
cd /tmp
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip && sudo ./aws/install
aws configure
```

## Install EKSCTL

## Install EKSCTL for Windows using Chocolatey 

```choco install eksctl ```

##  Install EKSCTL for Linux
```bash
curl --silent --location "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp
sudo mv /tmp/eksctl /usr/local/bin
eksctl version
```

## Checking AWS STS access to get role ARN for current session

```bash
aws sts get-caller-identity --query Account --output text --profile $PROFILE
```

```bash
export REGION=us-east-1
export PROFILE=<YOUR-PROFILE>
export ID=`aws sts get-caller-identity --query Account --output text --profile $PROFILE`
export ENVMODE=test
export CLUSTER=<YOUR-CLUSTER>
```


**Source:**

<https://kubernetes.github.io/ingress-nginx/deploy/>

<https://docs.aws.amazon.com/eks/latest/userguide/eks-managing.html>

<https://aws.amazon.com/pt/premiumsupport/knowledge-center/eks-kubernetes-services-cluster/>

<https://github.com/kubenav/kubenav>
