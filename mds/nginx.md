## Deploy NGINX

kubectl create deployment nginx --image=nginx

You can now list the Nginx deployment with the following command:

kubectl get deployments

You should see the following output:

![image](https://user-images.githubusercontent.com/40394399/120726578-c39afc80-c4ae-11eb-9e15-2f4f5ffe3af0.png)

Once the Nginx has been deployed, the application can be exposed with the following command:

kubectl create service nodeport nginx --tcp=80:80

You can now see a new Service and ClusterIP address assigned with the following command:

kubectl get svc

![image](https://user-images.githubusercontent.com/40394399/120726652-f7762200-c4ae-11eb-9c6b-b6db4796dd0b.png)
