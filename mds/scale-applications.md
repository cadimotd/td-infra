
## SCALE APPLICATION USING KUBECTL

## Enable Auto Scaling

```bash
wget -c https://raw.githubusercontent.com/kubernetes/autoscaler/master/cluster-autoscaler/cloudprovider/aws/examples/cluster-autoscaler-autodiscover.yaml
```

On line **157** replace **YOUR CLUSTER NAME** with your cluster name.

```bash
vim -c cluster-autoscaler-autodiscover.yaml
```

```bash
kubectl apply -f cluster-autoscaler-autodiscover.yaml
```

### Deploy a test application

```bash
kubectl apply -f https://raw.githubusercontent.com/smashse/playbook/master/HOWTO/KUBERNETES/COMBO/example_combo_full.yaml
```

```bash
kubectl get nodes -o wide
```

```bash
kubectl get pods -o=custom-columns=NODE:.spec.nodeName,NAME:.metadata.name --namespace teste
```
## Scale Applications 
Example : Scale in or scale out - many replicas for possible.

```bash
kubectl scale deployment teste-deployment --replicas=10 --namespace teste
```

```bash
kubectl get pods -o=custom-columns=NODE:.spec.nodeName,NAME:.metadata.name --namespace teste
```

Source: https://eksctl.io/usage/autoscaling/ https://github.com/kubernetes/autoscaler/blob/master/cluster-autoscaler/cloudprovider/aws/README.md
