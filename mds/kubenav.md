
## Deploy Kubenav

kubenav is the navigator for your Kubernetes clusters right in your pocket. kubenav is a mobile, desktop and web app to manage Kubernetes clusters and to get an overview of the status of your resources.



```bash
kubectl apply --kustomize github.com/kubenav/deploy/kustomize
kubectl port-forward --namespace kubenav svc/kubenav 14122
```
