
## Deploy the Metrics Server

```bash
kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml
```

```bash
kubectl get deployment metrics-server -n kube-system
```

## Deploy Prometheus using Helm

```bash
helm repo add stable https://charts.helm.sh/stable
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
```

```bash
helm install prometheus prometheus-community/prometheus \
--namespace prometheus \
--create-namespace \
--set alertmanager.persistentVolume.storageClass="gp2",server.persistentVolume.storageClass="gp2"
```

```bash
kubectl get pods -n prometheus
```

### Get the PushGateway URL by running these commands in the same shell

```bash
export POD_NAME=$(kubectl get pods --namespace prometheus -l "app=prometheus,component=pushgateway" -o jsonpath="{.items[0].metadata.name}")
kubectl --namespace prometheus port-forward $POD_NAME 9091
```

## Deploy Grafana using Helm

```bash
helm repo add stable https://charts.helm.sh/stable
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update
```

```bash
helm install grafana stable/grafana \
--namespace grafana \
--create-namespace \
--set grafana.persistentVolume.storageClass="gp2",server.persistentVolume.storageClass="gp2"
```

OR

```bash
helm repo add stable https://charts.helm.sh/stable
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
```

```bash
helm install grafana bitnami/grafana \
--namespace grafana \
--create-namespace \
--set grafana.persistentVolume.storageClass="gp2",server.persistentVolume.storageClass="gp2"
```

```bash
kubectl get pods -n grafana
```

### Get the application URL by running these commands

```bash
echo "Browse to http://127.0.0.1:8080"
kubectl port-forward svc/grafana 8080:3000 &
```

### Delete Pods 

```bash
kubectl delete --all deployments --namespace=foo
```




**Kubernetes Dashboard Examples:** <https://grafana.com/orgs/itewk/dashboards>
