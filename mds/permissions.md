  

### Get the admin credentials

```bash
echo "User: admin"
echo "Password: $(kubectl get secret grafana-admin --namespace grafana -o jsonpath="{.data.GF_SECURITY_ADMIN_PASSWORD}" | base64 --decode)"
```

## Expose Services

```bash
kubectl expose deployment prometheus-server --namespace prometheus --type=LoadBalancer --name=prometheus-loadbalancer
kubectl expose deployment grafana --namespace grafana --type=LoadBalancer --name=grafana-loadbalancer
kubectl expose deployment kubenav --namespace kubenav --type=LoadBalancer --name=kubenav-loadbalancer
```

## Show EXTERNAL-IP

```bash
kubectl get service/prometheus-loadbalancer --namespace prometheus |  awk {'print $1" " $2 " " $4 " " $5'} | column -t
kubectl get service/grafana-loadbalancer --namespace grafana |  awk {'print $1" " $2 " " $4 " " $5'} | column -t
kubectl get service/kubenav-loadbalancer --namespace kubenav |  awk {'print $1" " $2 " " $4 " " $5'} | column -t
```

OR

```bash
kubectl get services --all-namespaces | grep loadbalancer | awk {'print $1" " $2 " " $4 " " $5'} | column -t
```
