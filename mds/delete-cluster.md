## Delete EKS cluster

```bash
eksctl delete cluster --name $CLUSTER --region $REGION --profile $PROFILE
kubectl config delete-context $PROFILE@$CLUSTER.$REGION.eksctl.io
kubectl config delete-context arn:aws:eks:$REGION:$ID:cluster/$CLUSTER
```
